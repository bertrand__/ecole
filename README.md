des trucs d'école...

Rappel à propos des PDF à compresser : `ghostscript -sDEVICE=pdfwrite -dPDFSETTINGS=/ebook -dNOPAUSE -dQUIET -dBATCH -sOutputFile=output.pdf input.pdf`

## ressources en ligne

- des [coloriages](https://www.flickr.com/photos/155759063@N06/albums/72157668748737459) issus des collections patrimoniales des bibliothèques
- des [coloriages](http://dessinemoiunehistoire.net) de dessins de peintres
- des [cours en ligne gratuits](https://www.toutapprendre.com/) pour tous les niveaux
- des [livres audio](https://bibliotheques.paris.fr/numerique/search.aspx#/Search/(query:(ExceptTotalFacet:!t,ForceSearch:!f,HiddenFacetFilter:%7B%7D,Page:0,PageRange:3,QueryGuid:'26e245e8-e306-4bf2-9298-dcede2f58862',QueryString:'Selections_exact:(%2226%7C_%7CAudio%202%22)',ResultSize:20,ScenarioCode:ebook,ScenarioDisplayMode:display-standard,SearchContext:14,SearchLabel:'Recherche%20par%20rebond',SearchTerms:'Selections_exact%2026%20_%20Audio%202',SortField:!n,SortOrder:0,TemplateParams:(Scenario:'',Scope:numerique,Size:!n,Source:'',Support:''),UseSpellChecking:!n))) accessibles par la bibliothèque numérique de Paris
- une super [liste de trucs à faire quand on est confinés](https://taleming.com/occuper-enfants-maison-coronavirus/)
- faire un [jeu de dessin hybride](http://www.bouletcorp.com/2020/03/16/hybrides-le-jeu/)
- des films et cours en accès libre : http://www.openculture.com/
- des [posters de l'éducation nationale](https://padlet.com/emi1paris/7ayo2gmwcf5k)
- une [sélection de films](https://www.troiscouleurs.fr/curiosity-by-mk2/) par MK2